Feature: Complete the sign off on new Zopa web browser

  Scenario: Navigate to Zopa site and complete the application form without create account

    Given User clicks zopa website
    And User select 'Get a Zopa Loan'
    And Click on 'Get my personalised rates' button
    When Complete the application form with the following details
      | email                 | Full name  | mobileNumber | dob        | postcode | annualIncome | amount |
      | rblah.blahr@gmail.com | Zopa co.uk | random()     | 10-10-1985 | E15 4AJ  | 50000        | 950    |
    Then The input data which user  on the corresponding fields to be displayed accordingly