Feature: Zopa apply loan error scenarios

  Scenario Outline: Apply a loan - error scenarios

    Given User clicks zopa website
    And User select 'Get a Zopa Loan'
    And Click on 'Get my personalised rates' button
    When Complete the application form with the following and  handle errors
      | email   | Full name   | mobileNumber   | dob   | postcode   | annualIncome | amount        |
      | <email> | <Full name> | <mobileNumber> | <dob> | <postcode> | <income>     | <loan amount> |

    Then Framework Validation fails with the following condition <error>
    Examples:
      | email                 | Full name  | mobileNumber | dob        | postcode | income | loan amount | error                   |
      | rblah.blahrgmail.com  | Zopa co.uk | random()     | 10-10-1985 | E15 4AJ  | 50000  | 950         | Email id not valid      |
      | rblah.blahr@gmail.com | Zopaco.uk  | random()     | 10-10-1985 | E15 4AJ  | 50000  | 950         | Invalid name            |
      | rblah.blahr@gmail.com | Zopa co.uk | 0789         | 10-10-1985 | E15 4AJ  | 50000  | 950         | mobile number not valid |
      | rblah.blahr@gmail.com | Zopa co.uk | random()     | 10-10-1985 | E15      | 50000  | 950         | postcode not valid      |