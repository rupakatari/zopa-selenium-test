package net.ruvetech.bdd;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.google.inject.Inject;
import cucumber.runtime.java.guice.ScenarioScoped;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.ruvetech.pom.ApplicationDetailsPage;
import net.ruvetech.pom.SignInPage;
import net.ruvetech.pom.ZopaLoansPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

@ScenarioScoped
public class ZopaSignOffStepDefinition {

    private String WEB_URL = "http://www.zopa.com";

    private SignInPage signInPage;
    private ZopaLoansPage zopaLoansPage;
    private ApplicationDetailsPage applicationDetailsPage;
    private String errorMessage;

    private WebDriver driver;

    @Inject
    public ZopaSignOffStepDefinition(ScenarioContainer scenarioContainer) {
        driver = scenarioContainer.webDriver;
        signInPage = new SignInPage(driver);
        zopaLoansPage = new ZopaLoansPage(driver);
        applicationDetailsPage = new ApplicationDetailsPage(driver);
    }

    @Given("^User clicks zopa website$")
    public void user_click_on_www_zopa_com() throws Throwable {
        driver.navigate().to(WEB_URL);
        String currentWindowHandler = driver.getWindowHandle();
        driver.switchTo().window(currentWindowHandler);

    }

    @Given("User select {string}")
    public void user_select_Get_a_Zopa_Loan(String string) throws Throwable {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        signInPage.clickOnZopaLoanButton();
    }

    @Given("Click on {string} button")
    public void click_on_Get_my_personalised_rates_button(String string) throws Throwable {
        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        zopaLoansPage.movetoPersonalisedRatebutton();

        Thread.sleep(3000);
        zopaLoansPage.clickOnPersonalisedRatesButton();
    }

    @When("Complete the application form with the following details")
    public void complete_the_application_form(DataTable dataTable) throws Throwable {

        List<List<String>> userInputData = dataTable.asLists();
        List<String> secondRow = userInputData.get(1);
        String emailId = secondRow.get(0);
        String fullName = secondRow.get(1); // full name
        String phoneNumber = secondRow.get(2); //phoneNumber
        String dob = secondRow.get(3); // dob
        String postCode = secondRow.get(4); // postcode
        String salary = secondRow.get(5); //annualIncome
        String mortgage = secondRow.get(6); //amount

        if (phoneNumber != null && phoneNumber.endsWith("()")) {
            if (phoneNumber.startsWith("random")) {
                int random6digit = (100000 + new Random().nextInt(899999));
                phoneNumber = "07000" + random6digit;
            } else {
                throw new RuntimeException("Invalid phone number");
            }

        }
        String firstName = null;
        String lastName = null;
        if (fullName != null && fullName.contains(" ")) {
            firstName = fullName.substring(0, fullName.indexOf(" "));
            lastName = fullName.substring(fullName.indexOf(" ") + 1, fullName.length());
        } else {
            throw new RuntimeException("Invalid name");
        }

        String dateOfBirthDay = null;
        String dateOfBirthMonth = null;
        String dateOfBirthYear = null;

        if (dob != null && dob.indexOf("-") != dob.lastIndexOf("-") && dob.indexOf("-") > -1) {
            dateOfBirthDay = dob.substring(0, dob.indexOf("-"));
            dateOfBirthMonth = dob.substring(dob.indexOf("-") + 1, dob.lastIndexOf("-"));
            dateOfBirthYear = dob.substring(dob.lastIndexOf("-") + 1, dob.length());
        } else {
            throw new RuntimeException("Invalid data of birth");
        }

        applicationDetailsPage.fillingApplicationForm(emailId, firstName, lastName, phoneNumber,
                dateOfBirthDay, dateOfBirthMonth, dateOfBirthYear, postCode, salary, mortgage);

    }

    @When("^Complete the application form with the following and  handle errors$")
    public void complete_the_application_form_and_handle_errors(DataTable dataTable) throws Throwable {
        errorMessage = null;
        try {
            complete_the_application_form(dataTable);
        } catch (RuntimeException e) {
            errorMessage = e.getMessage();
        }
    }

    @Then("The input data which user  on the corresponding fields to be displayed accordingly")
    public void the_input_data_which_user_on_the_corresponding_fields_to_be_displayed_accordingly() throws Throwable {
        System.out.println("Application successfully created");
    }

    @Then("^Framework Validation fails with the following condition (.*)$")
    public void framework_Validation_fails_with_the_following_condition(String errMessage) throws Throwable {
        Assert.assertEquals(errMessage, errorMessage);
    }
}
