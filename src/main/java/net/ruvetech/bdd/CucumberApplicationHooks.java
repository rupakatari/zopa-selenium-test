package net.ruvetech.bdd;

import com.google.inject.Inject;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

public class CucumberApplicationHooks {

    static {
        System.setProperty("webdriver.chrome.driver", "/Users/venugopalv/dev-workspace/practice/zopa-selenium-test/chromedriver");
    }

    @Inject
    ScenarioContainer scenarioContainer;

    @Before
    public void beforeEveryMethod() {
        scenarioContainer.webDriver = new ChromeDriver();
    }

    @After
    public void afterEveryTest() {
        //scenarioContainer.webDriver.quit();
    }


}
