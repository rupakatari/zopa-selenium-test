package net.ruvetech.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SignInPage extends BasePage {

    @FindBy(how = How.XPATH, using = ".//*[@id='homepage-hero']/div/div/div/div[1]/a")
    WebElement getAZopaLoanFieldButton;


    public SignInPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    public void clickOnZopaLoanButton() {
        getAZopaLoanFieldButton.click();

    }
}
