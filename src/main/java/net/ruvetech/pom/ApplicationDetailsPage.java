package net.ruvetech.pom;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ApplicationDetailsPage extends BasePage {

    @FindBy(how = How.XPATH, using = ".//*[@id='member_email']")
    private WebElement emailTextField;

    @FindBy(how = How.XPATH, using = ".//*[@id='content']/div/div[1]/div/form/fieldset[1]/div[2]/p[1]/span[4]")
    private WebElement radioButtonField;

    @FindBy(how = How.XPATH, using = ".//*[@id='applications_loan_apply_first_name']")
    private WebElement firstNameTextField;

    @FindBy(how = How.XPATH, using = ".//*[@id='applications_loan_apply_last_name']")
    private WebElement lastNameTextField;

    @FindBy(how = How.XPATH, using = ".//*[@id='applications_loan_apply_home_phone']")
    private WebElement phoneNumberTextField;

    @FindBy(how = How.XPATH, using = ".//*[@id='date_of_birth_day']")
    private WebElement dateOfBirthDate;

    @FindBy(how = How.XPATH, using = ".//*[@id='date_of_birth_month']")
    private WebElement dateOfBirthMonth;

    @FindBy(how = How.XPATH, using = ".//*[@id='date_of_birth_year']")
    private WebElement dateOfBirthYear;

    @FindBy(how = How.XPATH, using = ".//*[@id='content']/div/div[1]/div/form/fieldset[1]/div[5]/p[1]/span[3]/label")
    private WebElement homeImprovementsRadioButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='address_postcode']")
    private WebElement addressPostcodeTextField;

    @FindBy(how = How.XPATH, using = ".//*[@id='address-lookup']/p[2]/input")
    private WebElement clickOnLookUpAddressButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='address_possible_address']/option[4]")
    private WebElement addressDropDown;

    @FindBy(how = How.XPATH, using = ".//*[@id='address_from_2i']")
    private WebElement monthDropDownField;

    @FindBy(how = How.CSS, using = "#address_from_1i")
    private WebElement yearDropDownField;

    @FindBy(how = How.XPATH, using = ".//*[@id='content']/div/div[1]/div/form/fieldset[2]/div/div/p[3]/input[1]")
    private WebElement useThisAddressField;

    @FindBy(how = How.XPATH, using = ".//*[@id='content']/div/div[1]/div/form/fieldset[3]/div[1]/p/span[2]/label")
    private WebElement empFullTimeRadioButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='applications_loan_apply_salary']")
    private WebElement annualIncomeBeforeTaxTextField;

    @FindBy(how = How.XPATH, using = ".//*[@id='content']/div/div[1]/div/form/fieldset[3]/div[3]/p[1]/span[3]/label")
    private WebElement yesWithMortgageRadioButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='applications_loan_apply_rent']")
    private WebElement rentTextField;


    public ApplicationDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    public void fillingApplicationForm(String email, String firstName, String lastName, String phoneNumber,
                                       String dobDate, String dobMonth, String dobYear, String postCode, String income, String mortgage) {

        if (null != email) {
            String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(email);
            if (!matcher.matches()) {
                throw new RuntimeException("Email id not valid");
            }
        }

        if (null != phoneNumber) {
            //
            String regex = "^(\\+44\\s?7\\d{3}|\\(?07\\d{3}\\)?)\\s?\\d{3}\\s?\\d{3}$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(phoneNumber);
            if (!matcher.matches()) {
                throw new RuntimeException("mobile number not valid");
            }
        }

        if (null != postCode) {
            String regex = "^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(postCode);
            if (!matcher.matches()) {
                throw new RuntimeException("postcode not valid");
            }
        }

        emailTextField.sendKeys(email);
        radioButtonField.click();
        firstNameTextField.sendKeys(firstName);
        lastNameTextField.sendKeys(lastName);
        phoneNumberTextField.sendKeys(phoneNumber);
        dateOfBirthDate.sendKeys(dobDate);
        dateOfBirthMonth.sendKeys(dobMonth);
        dateOfBirthYear.sendKeys(dobYear);
        homeImprovementsRadioButton.click();
        addressPostcodeTextField.sendKeys(postCode);
        clickOnLookUpAddressButton.click();

        Actions addressDropDownActions = new Actions(webDriver);
        addressDropDownActions.moveToElement(addressDropDown).click().perform();
        addressDropDown.click();

        Actions monthDropdownActions = new Actions(webDriver);
        monthDropdownActions.moveToElement(monthDropDownField).click().perform();
        Select selectMonth = new Select(monthDropDownField);
        selectMonth.selectByVisibleText("November");

        Actions yearDropdownActions = new Actions(webDriver);
        yearDropdownActions.moveToElement(yearDropDownField).click().perform();
        Select selectYear = new Select(yearDropDownField);
        selectYear.selectByVisibleText("2011");

        useThisAddressField.click();
        empFullTimeRadioButton.click();
        annualIncomeBeforeTaxTextField.sendKeys(income);
        yesWithMortgageRadioButton.click();
        rentTextField.sendKeys(mortgage);


    }

}
