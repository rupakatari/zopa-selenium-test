package net.ruvetech.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ZopaLoansPage extends BasePage {


    @FindBy(how = How.ID, using = "submit-loan-button")
    WebElement getMyPersonalisedRatesButton;

    public ZopaLoansPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void movetoPersonalisedRatebutton() {

        Actions actions = new Actions(webDriver);
        actions.moveToElement(getMyPersonalisedRatesButton).click().perform();
    }

    public void clickOnPersonalisedRatesButton() {
        getMyPersonalisedRatesButton.click();
    }

}
